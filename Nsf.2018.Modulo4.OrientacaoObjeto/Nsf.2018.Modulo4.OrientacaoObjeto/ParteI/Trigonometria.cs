﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo4.OrientacaoObjeto.ParteI
{
    class Trigonometria
    {
        public decimal CalcularAreaRetangulo(decimal largura, decimal altura)
        {
            decimal area = 0;
            area = largura * altura;

            return area;
        }

        public decimal CalcularAreaTriangulo(decimal largura, decimal altura)
        {
            decimal area = 0;
            area = (largura * altura) / 2;

            return area;
        }
    }
}
