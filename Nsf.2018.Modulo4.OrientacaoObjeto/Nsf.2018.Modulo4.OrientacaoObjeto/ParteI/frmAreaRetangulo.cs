﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmAreaRetangulo : Form
    {
        public frmAreaRetangulo()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal largura = Convert.ToDecimal(txtBase.Text);
            decimal altura = Convert.ToDecimal(txtAltura.Text);

            Trigonometria trig = new Trigonometria();
            decimal area = trig.CalcularAreaRetangulo(largura, altura);

            lblArea.Text = area.ToString();
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {
            frmAreaTriangulo frm = new frmAreaTriangulo();
            frm.Show();
            Hide();
        }
    }
}
