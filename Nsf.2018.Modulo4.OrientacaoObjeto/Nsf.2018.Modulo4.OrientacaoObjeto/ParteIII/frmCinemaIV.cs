﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using Nsf._2018.Modulo4.OrientacaoObjeto.ParteIII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaIV : Form
    {
        public frmCinemaIV()
        {
            InitializeComponent();
            cboTipoIngresso.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                decimal valor = Convert.ToDecimal(txtValor.Text);
                string nome = txtNome.Text;
                string rg = txtRG.Text;
                string tipoIngresso = cboTipoIngresso.SelectedItem.ToString();

                CalculadoraIngresso2 calculadoraIngressos = new CalculadoraIngresso2();
                decimal total = calculadoraIngressos.CalcularIngressoUnitario(valor, nome, rg, tipoIngresso);

                lblTotal.Text = total.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {

        }
    }
}
