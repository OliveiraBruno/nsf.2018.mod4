﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo4.OrientacaoObjeto.ParteIII
{
    class CalculadoraIngresso2
    {
        public decimal CalcularIngressos(decimal valor, int qtdInteira, int qtdMeia, int qtdAniversariante)
        {
            decimal totalInteiras = CalcularIngressosInteira(valor, qtdInteira);
            decimal totalMeias = CalcularIngressosMeia(valor, qtdMeia);
            decimal totalAniversariantes = CalcularIngressosAniversariante(valor, qtdAniversariante);

            decimal total = totalInteiras + totalMeias + totalAniversariantes;
            return total;
        }


        private decimal CalcularIngressosInteira(decimal valor, int qtd)
        {
            decimal total = valor * qtd;

            total = Math.Round(total, 2);

            return total;
        }


        private decimal CalcularIngressosMeia(decimal valor, int qtd)
        {
            decimal total = (valor / 2) * qtd;

            total = Math.Round(total, 2);

            return total;
        }


        private decimal CalcularIngressosAniversariante(decimal valor, int qtd)
        {
            decimal total = (valor * 0.3m) * qtd;

            total = Math.Round(total, 2);

            return total;
        }


        public decimal CalcularIngressoUnitario(decimal valor, string nome, string rg, string tipoIngresso)
        {
            ValidarIngresso(nome, rg, tipoIngresso);

            decimal total = 0;
            switch (tipoIngresso)
            {
                case "Inteira":
                    total = CalcularIngressosInteira(valor, 1);
                    break;

                case "Meia":
                    total = CalcularIngressosMeia(valor, 1);
                    break;

                case "Aniversariante":
                    total = CalcularIngressosAniversariante(valor, 1);
                break;
            }

            return total;
        }


        private void ValidarIngresso(string nome, string rg, string tipoIngresso)
        {
            if (tipoIngresso == "Aniversariante" || tipoIngresso == "Meia")
            {
                if (nome == string.Empty)
                {
                    throw new ArgumentException("Nome é obrigatório.");
                }

                if (rg == string.Empty)
                {
                    throw new ArgumentException("RG é obrigatório.");
                }
            }
        }
    }
}
