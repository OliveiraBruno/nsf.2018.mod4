﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using Nsf._2018.Modulo4.OrientacaoObjeto.ParteIII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaIII : Form
    {
        public frmCinemaIII()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal(txtValor.Text);
            int qtdInteira = Convert.ToInt32(txtQtdInteiras.Text);
            int qtdMeias = Convert.ToInt32(txtQtdMeias.Text);
            int qtdAniversariante = Convert.ToInt32(txtQtdNiver.Text);

            CalculadoraIngresso2 calculadoraIngressos = new CalculadoraIngresso2();
            decimal total = calculadoraIngressos.CalcularIngressos(valor, qtdInteira, qtdMeias, qtdAniversariante);

            lblTotal.Text = total.ToString();
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {
            frmCinemaIV tela = new frmCinemaIV();
            tela.Show();
            Hide();
        }
    }
}
