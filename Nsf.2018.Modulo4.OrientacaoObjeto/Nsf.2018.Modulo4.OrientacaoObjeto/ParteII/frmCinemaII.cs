﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaII : Form
    {
        public frmCinemaII()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal(txtValor.Text);
            int qtdInteira = Convert.ToInt32(txtQtdInteiras.Text);
            int qtdMeias = Convert.ToInt32(txtQtdMeias.Text);

            CalculadoraIngressos calculadoraIngressos = new CalculadoraIngressos();
            decimal total = calculadoraIngressos.CalcularIngressos(valor, qtdInteira, qtdMeias);

            lblTotal.Text = total.ToString();
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {
            frmCinemaIII tela = new frmCinemaIII();
            tela.Show();
            Hide();
        }
    }
}
