﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaI : Form
    {
        public frmCinemaI()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal(txtValor.Text);
            int qtdInteira = Convert.ToInt32(txtQuantidade.Text);

            CalculadoraIngressos calculadoraIngressos = new CalculadoraIngressos();
            decimal total = calculadoraIngressos.CalcularIngressosInteira(valor, qtdInteira);

            lblTotal.Text = total.ToString();
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {
            frmCinemaII tela = new frmCinemaII();
            tela.Show();
            Hide();
        }
    }
}
